import { ref, watch, computed } from "vue";
import { useRoute, useRouter } from "vue-router";
import { useQuasar } from "quasar";
import Filters from "src/components/Filters.vue";
import MainDialog from "src/components/MainDialog.vue";
import DeleteModal from "../components/DeleteModal.vue";
import { savedTodoForm } from "../CreateTodoList";
import { ToggleMainDialogState } from "../../../composables/Triggers";

export const selectedItem = ref([
  {
    task_title: "",
    date: "",
    tasks: [],
  },
]);

export const currentIndex = ref(null);

export default {
  components: {
    Filters,
    MainDialog,
    DeleteModal,
  },
  setup() {
    const $q = useQuasar();
    const route = useRoute();
    const router = useRouter();

    let pageLoadingState = ref(false);
    let btnLoadingState = ref(false);

    const showDeleteDialog = () => {
      ToggleMainDialogState();
    };

    const showNotify = () => {
      let status = true;
      $q.notify({
        position: $q.screen.width < 767 ? "top" : "bottom-right",
        classes: `${
          status ? "onboarding-success-notif" : "onboarding-error-notif"
        } q-px-lg q-pt-none q-pb-none`,
        html: true,
        message: status
          ? `<div class="text-bold">Successfully Deleted!</div> To-do List has been deleted successfully.`
          : `<div class="text-bold">Failed!</div> Failed to delete a new To-do List`,
      });
    };

    return {
      route,
      router,
      pageLoadingState,
      btnLoadingState,
      savedTodoForm,
      showDeleteDialog,
      showNotify,
    };
  },
  provide() {
    return {
      deleteTodo: this.deleteTodo,
    };
  },
  data() {
    return {
      todoList: [
        {
          task_title: "To-Do List",
          date: "April 05, 2024",
          tasks: [
            {
              checkbox: false,
              task_name: "Make To-do List design",
              time: "8:00 AM",
            },
            {
              checkbox: false,
              task_name: "Fill up OJT Evaluation Form ",
              time: "12:00 PM",
            },
            {
              checkbox: false,
              task_name: "Update Sop For OJT tardiness",
              time: "1:00 PM",
            },
            {
              checkbox: false,
              task_name: "Check  BDD userstory",
              time: "5:00 PM",
            },
          ],
        },
        {
          task_title: "Onboarding Task",
          date: "April 06, 2024",
          tasks: [
            {
              checkbox: false,
              task_name: "Create A New Task",
              time: "10:00 AM",
            },
            {
              checkbox: false,
              task_name: "Update Selected Task",
              time: "11:00 AM",
            },
          ],
        },
      ],
      doneList: [],
      showMenu: [],
    };
  },
  mounted() {
    this.doneList = this.todoList.map((task) => ({
      ...task,
      tasks: [],
    }));

    watch(
      this.selectedItem,
      (newValue, oldValue) => {
        console.log("selectedItem changed:", newValue);
      },
      { deep: true }
    );

    if (savedTodoForm.value.length > 0) {
      savedTodoForm.value.forEach((newTask) => {
        const existingTaskIndex = this.todoList.findIndex(
          (task) =>
            task.task_title === newTask.task_title && task.date === newTask.date
        );

        if (existingTaskIndex === -1) {
          this.todoList.unshift(newTask);

          const doneTask = {
            task_title: newTask.task_title,
            date: newTask.date,
            tasks: [],
          };
          this.doneList.unshift(doneTask);
        }
      });
    }
    if (selectedItem.value) {
      this.todoList[currentIndex.value] = Object.assign(
        {},
        {
          task_title: selectedItem.value.task_title,
          date: selectedItem.value.date,
          tasks: selectedItem.value.tasks,
        }
      );

      if (
        this.doneList[currentIndex.value] &&
        this.doneList[currentIndex.value].task_title !==
          selectedItem.value.task_title
      ) {
        this.doneList[currentIndex.value].task_title =
          selectedItem.value.task_title;
      }

      selectedItem.value = {};

      console.log("Updated todoList:", this.todoList);
      console.log("Cleared selectedItem:", selectedItem.value);
    }
  },

  watch: {
    todoList: {
      handler(newTodoList) {
        newTodoList.forEach((task) => {
          const foundIndex = this.doneList.findIndex(
            (doneTask) =>
              doneTask.task_title === task.task_title &&
              doneTask.date === task.date
          );
          console.log("foundIndex: ", foundIndex);
          if (foundIndex !== -1) {
            task.tasks.forEach((keyTask, index) => {
              if (keyTask.checkbox) {
                this.doneList[foundIndex].tasks.push(keyTask);
                this.todoList
                  .find(
                    (savedTask) =>
                      savedTask.task_title === task.task_title &&
                      savedTask.date === task.date
                  )
                  .tasks.splice(index, 1);
              }
              console.log("DoneTasks:", this.doneList);
            });
          }
        });
      },
      deep: true,
    },
    doneList: {
      handler(newDoneList) {
        newDoneList.forEach((task) => {
          const foundIndex = this.todoList.findIndex(
            (savedTask) =>
              savedTask.task_title === task.task_title &&
              savedTask.date === task.date
          );
          if (foundIndex !== -1) {
            task.tasks.forEach((keyTask, index) => {
              if (!keyTask.checkbox) {
                this.todoList[foundIndex].tasks.push(keyTask);
                this.doneList
                  .find(
                    (doneTask) =>
                      doneTask.task_title === task.task_title &&
                      doneTask.date === task.date
                  )
                  .tasks.splice(index, 1);
              }
              console.log("Todo List:", this.todoList);
            });
          }
        });
      },
      deep: true,
    },
  },

  methods: {
    toggleMenu(index) {
      console.log(this.showMenu);
      console.log(index);
      currentIndex.value = index;
    },
    updateTodo() {
      const taskIndex = currentIndex.value;
      const task = this.todoList[taskIndex];
      if (task) {
        // this.selectedItem = task;
        selectedItem.value = task;
        console.log("selectedItemTodo:", selectedItem.value);
        this.$router.push({ name: "update-todo", params: { id: taskIndex } });
      } else {
        console.error("Task not found at index:", currentIndex.value);
      }
    },
    deleteTodo(acceptDelete) {
      if (acceptDelete) {
        const indexToDelete = currentIndex.value;

        if (indexToDelete !== null && indexToDelete !== -1) {
          this.todoList.splice(indexToDelete, 1);
          this.doneList.splice(indexToDelete, 1);
        }
        this.showNotify(true);
      } else {
        this.showNotify(false);
      }
    },
  },
};
