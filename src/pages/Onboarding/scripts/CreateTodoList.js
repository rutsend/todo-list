import { ref } from "vue";
import { useQuasar } from "quasar";
import { useRoute, useRouter } from "vue-router";
import VueHighcharts from "vue3-highcharts";

export const savedTodoForm = ref([]);
export const addNewTodo = (task_title, form, router, showNotify) => {
  const currentDate = new Date();
  const options = { month: "long", day: "numeric", year: "numeric" };
  const formattedDate = currentDate.toLocaleDateString("en-US", options);

  // Validate form fields

  const newTodoList = {
    task_title: task_title ? task_title.value : null,
    date: formattedDate,
    tasks: form
      ? form.value.map((task) => {
          return {
            checkbox: false,
            task_name: task.task_name,
            time: task.time,
          };
        })
      : null,
  };

  // Save the todo list
  savedTodoForm.value.push(newTodoList);
  console.log(savedTodoForm.value);

  // Show success notification
  if (showNotify && typeof showNotify === "function") {
    showNotify(true);
  } else {
    showNotify(false);
  }

  // Redirect to todo list page
  router.push("todo-list");
};
export default {
  components: {
    VueHighcharts,
  },
  setup() {
    const $q = useQuasar();
    const route = useRoute();
    const router = useRouter();
    let pageLoadingState = ref(false);
    let onTimePopup = ref(null);
    let task_title = ref("");
    let form = ref([
      {
        task_name: null,
        time: "",
      },
    ]);
    const addTodo = () => {
      form.value.push({
        task_name: null,
        time: "",
      });
    };

    const removeTodo = (index) => {
      if (index > -1) {
        form.value.splice(index, 1);
      }
    };

    const setTimePopup = (index) => {
      onTimePopup.value = index;
    };

    const showNotify = () => {
      let status = true;
      $q.notify({
        position: $q.screen.width < 767 ? "top" : "bottom-right",
        classes: `${
          status ? "onboarding-success-notif" : "onboarding-error-notif"
        } q-px-lg q-pt-none q-pb-none`,
        html: true,
        message: status
          ? `<div class="text-bold">Success!</div> New To-do List has been added successfully.`
          : `<div class="text-bold">Failed!</div> Failed to add a new To-do List`,
      });
    };

    return {
      task_title,
      form,
      addTodo,
      removeTodo,
      route,
      router,
      pageLoadingState,
      onTimePopup,
      setTimePopup,
      showNotify,
      addNewTodo: () => addNewTodo(task_title, form, router, showNotify),
    };
  },
};
